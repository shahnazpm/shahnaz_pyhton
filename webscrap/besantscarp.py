from bs4 import BeautifulSoup
import requests

source= requests.get('https://www.besanttechnologies.com/').text
soup= BeautifulSoup(source, 'lxml')
# print(soup.prettify())

header= soup.find('title').text
print(header)
content= soup.find('script', class_='yoast-schema-graph yoast-schema-graph--main').text
print(content)
